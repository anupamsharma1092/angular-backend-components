export class Category{

    public name: string;
  
    public imagePath: string;

    public sub_categories: Category[];

    


    constructor(name: string,imagePath: string){
        this.name = name;
        this.imagePath = imagePath;
        this.sub_categories = [];
    }
   

    addSubCategory(subCat:Category){
        this.sub_categories.push(subCat);
    }
}