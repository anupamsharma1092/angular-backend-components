import { Component, OnInit } from '@angular/core';
import { Category } from '../category.model';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.css']
})
export class CategoriesListComponent implements OnInit {


  categories: Category[] =[
    new Category("A test Category","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg"),
    new Category("B test Category","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg"),
    new Category("C test Category","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg"),
    new Category("D test Category","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg"),
    new Category("E test Category","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg"),
    new Category("F test Category","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg"),
    new Category("E test Category","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg")
   
  ];
  
  constructor() { 

   
  }

  ngOnInit() {
  }

}
