export class Product{

    public name: string;
  
    public imagePath: string;

    public price: number;

    public showForDelivery: boolean;

    public showForPickup: boolean;

    public showForWalkin: boolean;

    constructor(name: string,imagePath: string,price: number){
        this.name = name;
        this.imagePath = imagePath;
        this.price = price;
        this.showForDelivery = true;
        this.showForPickup = true;
        this.showForWalkin = true;
    }

        
}