import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";

import { AppComponent } from './app.component';
import { CategoriesComponent } from './categories/categories.component';
import { CategoriesListComponent } from './categories/categories-list/categories-list.component';
import { CategoriesDetailComponent } from './categories/categories-detail/categories-detail.component';
import { NewCategoryComponent } from './categories/new-category/new-category.component';
import { ProductsComponent } from './categories/products/products.component';
import { ProductsListComponent } from './categories/products/products-list/products-list.component';
import { NewProductComponent } from './categories/products/new-product/new-product.component';
import { ProductDetailComponent } from './categories/products/product-detail/product-detail.component';
import { SubCategoriesComponent } from './categories/sub-categories/sub-categories.component';


const appRoutes: Routes = [
 
  {path: 'home', component: CategoriesComponent},
  {path: 'category-detail', component: CategoriesDetailComponent},
  {path: 'new-category', component: CategoriesDetailComponent},
  {path: 'products-list', component: ProductsListComponent},
  {path: 'product-detail', component: ProductDetailComponent},
  {path: 'new-product', component: NewProductComponent},
  {path: 'sub-categories', component: SubCategoriesComponent}
];



@NgModule({
  declarations: [
    AppComponent,
    CategoriesComponent,
    CategoriesListComponent,
    CategoriesDetailComponent,
    NewCategoryComponent,
    ProductsComponent,
    ProductsListComponent,
    NewProductComponent,
    ProductDetailComponent,
    SubCategoriesComponent
  ],
  imports: [
    BrowserModule,
    
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
