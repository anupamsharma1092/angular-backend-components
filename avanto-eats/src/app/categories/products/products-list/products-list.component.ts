import { Component, OnInit } from '@angular/core';
import { Product } from '../product.model';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {


  products: Product[] =[
    new Product("A test Product","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg",10),
    new Product("B test Product","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg",10),
    new Product("C test Product","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg",10),
    new Product("D test Product","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg",10),
    new Product("E test Product","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg",10),
    new Product("F test Product","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg",10),
    new Product("E test Product","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg",10)
   
  ];
  constructor() { }

  ngOnInit() {
  }

}
