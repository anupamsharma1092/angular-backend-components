import { Component, OnInit } from '@angular/core';
import { Category } from '../category.model';

@Component({
  selector: 'app-sub-categories',
  templateUrl: './sub-categories.component.html',
  styleUrls: ['./sub-categories.component.css']
})
export class SubCategoriesComponent implements OnInit {

  categories: Category[] =[
    new Category("A test sub-Category","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg"),
    new Category("B test sub-Category","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg"),
    new Category("C test sub-Category","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg"),
    new Category("D test sub-Category","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg"),
    new Category("E test sub-Category","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg"),
    new Category("F test sub-Category","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg"),
    new Category("E test sub-Category","https://cdn.pixabay.com/photo/2017/11/16/18/51/kagyana-2955466_960_720.jpg")
   
  ];
  constructor() { }

  ngOnInit() {
  }

}
